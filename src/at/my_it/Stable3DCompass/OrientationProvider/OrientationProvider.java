/**
 * 
 */
package at.my_it.Stable3DCompass.OrientationProvider;

import java.util.ArrayList;
import java.util.List;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import at.my_it.Stable3DCompass.Representation.Quaternion;

/**
 * Classes implementing this interface provide an orientation of the device either by directly accessing hardware, using
 * Android sensor fusion or fusing sensors itself.
 * 
 * The orientation can be provided as rotation matrix or quaternion.
 * 
 * @author Alexander Pacha
 * 
 */
public abstract class OrientationProvider implements SensorEventListener {
    /**
     * Sync-token for syncing read/write to sensor-data from sensor manager and fusion algorithm
     */
    protected final Object syncToken = new Object();

    /**
     * The list of sensors used by this provider
     */
    protected List<Sensor> sensorList = new ArrayList<Sensor>();

    /**
     * The quaternion that holds the current rotation
     */
    protected final Quaternion currentOrientationQuaternion;

    /**
     * The quaternion that is used to compensate for magnetic declination
     */
    protected final Quaternion correctionQuaternion;
    
    /**
     * The sensor manager for accessing android sensors
     */
    protected SensorManager sensorManager;
    
    /**
     * The magnetic declination based on your current position and time. See http://www.ngdc.noaa.gov/geomag/WMM/DoDWMM.shtml for more information.
     */
    protected float magneticDeclination;

    /**
     * Initialises a new OrientationProvider
     * 
     * @param sensorManager The android sensor manager
     * @param magneticDeclination The magnetic declination of your current position
     */
    public OrientationProvider(SensorManager sensorManager, float magneticDeclination) {
        this.sensorManager = sensorManager;
        this.magneticDeclination = magneticDeclination;

        // Initialise with identity
        currentOrientationQuaternion = new Quaternion();
   
        // Initialize with quaternion that is used to compensate magnetic declination (rotate around z-axis, which is azimuth)
        correctionQuaternion = new Quaternion();
        correctionQuaternion.setEulerAngle(0, 0, magneticDeclination);
    }

    /**
     * Starts the sensor fusion (e.g. when resuming the activity)
     */
    public void start() {
        // enable our sensor when the activity is resumed, ask for
        // 10 ms updates.
        for (Sensor sensor : sensorList) {
            // enable our sensors when the activity is resumed, ask for
            // 20 ms updates (Sensor_delay_game)
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
        }
    }

    /**
     * Stops the sensor fusion (e.g. when pausing/suspending the activity)
     */
    public void stop() {
        // make sure to turn our sensors off when the activity is paused
        for (Sensor sensor : sensorList) {
            sensorManager.unregisterListener(this, sensor);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Not doing anything
    }

    /**
     * @return Returns the current rotation of the device in the quaternion format (vector4f)
     */
    public Quaternion getQuaternion() {
        synchronized (syncToken) {
            Quaternion correctedQuaternion = new Quaternion();
            currentOrientationQuaternion.multiplyByQuat(correctionQuaternion, correctedQuaternion);            
            return correctedQuaternion;
        }
    }
}

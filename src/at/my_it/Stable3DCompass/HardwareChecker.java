package at.my_it.Stable3DCompass;

import android.hardware.Sensor;
import android.hardware.SensorManager;

/**
 * Class that tests availability of hardware sensors.
 * 
 * @author Alex
 * 
 */
public class HardwareChecker implements SensorChecker {

    /**
     * True, if gyroscope is available
     */
    boolean gyroscopeIsAvailable = false;

    /**
     * Concrete class to check the existing hardware
     * @param sensorManager The sensor manager to access the device's hardware
     */
    public HardwareChecker(SensorManager sensorManager) {
        if (sensorManager.getSensorList(Sensor.TYPE_GYROSCOPE).size() > 0) {
            gyroscopeIsAvailable = true;
        }
    }

    @Override
    public boolean isGyroscopeAvailable() {
        return gyroscopeIsAvailable;
    }

}

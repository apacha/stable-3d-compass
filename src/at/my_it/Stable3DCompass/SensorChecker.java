package at.my_it.Stable3DCompass;

/**
 * General interface for checking sensors
 * @author Alex
 *
 */
public interface SensorChecker {

    /**
     * Checks if the device that is currently running the application has a hardware gyroscope built into it.
     * 
     * @return True, if a gyroscope is available. False otherwise.
     */
    public boolean isGyroscopeAvailable();
}

package at.my_it.Stable3DCompass;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import at.my_it.Stable3DCompass.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.GeomagneticField;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import at.my_it.Stable3DCompass.OrientationProvider.ImprovedOrientationSensor1Provider;
import at.my_it.Stable3DCompass.OrientationProvider.ImprovedOrientationSensor2Provider;
import at.my_it.Stable3DCompass.OrientationProvider.OrientationProvider;
import at.my_it.Stable3DCompass.OrientationProvider.RotationVectorProvider;

/**
 * The main activity where the user can select which sensor-fusion he wants to try out
 * 
 * @author Alexander Pacha
 * 
 */
public class SensorSelectionActivity extends FragmentActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a {@link android.support.v4.app.FragmentPagerAdapter} derivative,
     * which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_selection);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // Check if device has a hardware gyroscope
        SensorChecker checker = new HardwareChecker((SensorManager) getSystemService(SENSOR_SERVICE));
        if (!checker.isGyroscopeAvailable()) {
            // If a gyroscope is unavailable, display a warning.
            displayHardwareMissingWarning();
        }
    }

    /**
     * Shows an alert box that the user gets informed that his device is missing a gyroscope.
     */
    private void displayHardwareMissingWarning() {
        AlertDialog ad = new AlertDialog.Builder(this).create();
        ad.setCancelable(false); // This blocks the 'BACK' button
        ad.setTitle(getResources().getString(R.string.gyroscope_missing));
        ad.setMessage(getResources().getString(R.string.gyroscope_missing_message));
        ad.setButton(DialogInterface.BUTTON_NEUTRAL, getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        ad.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sensor_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_about:
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        /**
         * Initialises a new sectionPagerAdapter
         * 
         * @param fm the fragment Manager
         */
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
            Fragment fragment = new OrientationVisualisationFragment();
            Bundle args = new Bundle();
            args.putInt(OrientationVisualisationFragment.ARG_SECTION_NUMBER, position + 1);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
            case 0:
                return getString(R.string.title_section1).toUpperCase(l);
            case 1:
                return getString(R.string.title_section2).toUpperCase(l);
            case 2:
                return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A fragment that contains the same visualisation for different orientation providers
     */
    public static class OrientationVisualisationFragment extends Fragment {
        /**
         * The surface that will be drawn upon
         */
        private GLSurfaceView mGLSurfaceView;
        /**
         * The class that renders the cube
         */
        private CubeRenderer mRenderer;
        /**
         * The current orientation provider that delivers device orientation.
         */
        private OrientationProvider currentOrientationProvider;

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        public static final String ARG_SECTION_NUMBER = "section_number";

        @Override
        public void onResume() {
            // Ideally a game should implement onResume() and onPause()
            // to take appropriate action when the activity looses focus
            super.onResume();
            currentOrientationProvider.start();
            mGLSurfaceView.onResume();
        }

        @Override
        public void onPause() {
            // Ideally a game should implement onResume() and onPause()
            // to take appropriate action when the activity looses focus
            super.onPause();
            currentOrientationProvider.stop();
            mGLSurfaceView.onPause();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            
            float declination = getMagneticDeclination();
            
            SensorManager sm = (SensorManager) getActivity().getSystemService(SENSOR_SERVICE);
            // Initialise the orientationProvider
            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
            case 1:
                currentOrientationProvider = new ImprovedOrientationSensor1Provider(sm, declination);
                break;
            case 2:
                currentOrientationProvider = new ImprovedOrientationSensor2Provider(sm, declination);
                break;
            case 3:
                currentOrientationProvider = new RotationVectorProvider(sm, declination);
                break;
            default:
                break;
            }

            // Create our Preview view and set it as the content of our Activity
            mRenderer = new CubeRenderer();
            mRenderer.setOrientationProvider(currentOrientationProvider);
            mGLSurfaceView = new GLSurfaceView(getActivity());
            mGLSurfaceView.setRenderer(mRenderer);

            return mGLSurfaceView;
        }

        /**
         * Retrieves the magnetic declination from the coarse position of the device. If not available, 0 will be returned.
         * @return The declination of the horizontal component of the magnetic field from true north, in degrees (i.e. positive means the magnetic field is rotated east that much from true north).
         */
        private float getMagneticDeclination() {
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            
            List<String> providers = locationManager.getAllProviders();
            
            if (providers.isEmpty()) {
                return 0;
            }

            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location == null) {
                return 0;
            }
            
            Date date = new Date();
            GeomagneticField field = new GeomagneticField((float) location.getLatitude(), (float) location.getLongitude(), (float) location.getAltitude(), date.getTime());            
            float declination = field.getDeclination();
            return declination;
        }
    }
}
